﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject choicePanel;
    public TextMeshProUGUI sfxText, bgmText;
    void Start()
    {
        choicePanel.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }

    public void ButtonClick()
    {
        if (Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("Click");
        }
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ContToUpgrade()
    {
        SceneManager.LoadScene(3);
    }
    public void BGMClick()
    {
        if (Menu.bgmOn)
        {
            bgmText.text = "OFF";
            Menu.bgmOn = false;
            FindObjectOfType<AudioManager>().Mute("Theme");
        }
        else
        {
            //FindObjectOfType<AudioManager>().Play("Click");
            bgmText.text = "ON";
            Menu.bgmOn = true;
            FindObjectOfType<AudioManager>().UnMute("Theme");
        }
    }
    public void SFXClick()
    {
        if (Menu.sfxOn)
        {
            sfxText.text = "OFF";
            Menu.sfxOn = false;
        }
        else
        {
            //FindObjectOfType<AudioManager>().Play("Click");
            sfxText.text = "ON";
            Menu.sfxOn = true;
        }
    }

    public void PauseButton()
    {
        Time.timeScale = 0;
    }

    public void Unpausebutton()
    {
        Time.timeScale = 1;
    }

}
