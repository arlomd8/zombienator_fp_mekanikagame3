﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using System.Threading;
using TMPro;

public class ZombieBehaviour : MonoBehaviour
{
    public static ZombieBehaviour instance;

    [Header("Zombie Attribute")]
    public int zombieLife;
    private int playerDamage;
    public int zombieDamage;
    public float moveSpeed;
    public float jumpForce;

    [Header("GameObject")]
    public TextMeshProUGUI lifeText;
    private GameObject z;
    private Rigidbody2D l;
    public Transform attackPos;
    public LayerMask whatIsEnemy;
    public GameObject humanWin;
    public Transform groundCheck;
    private GameObject land;
    private Transform postLand;
    private Rigidbody2D rb;
    public HealthBar healthBar;
    public GameObject contButton;

    [Header("Other")]
    private float timeBtwAttack;
    public float startTimeBtwAttack;
    public float attackRange;
    public float thrust;
    private float moveInput;
    private float jumpInput;
    public bool isGrounded = true;
    private bool facingRight = true;
    private Animator animator;
    public MultipleTargetCam camera;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }
    //void OnGUI()
    //{
    //    GUI.Label(new Rect(1000, 10, 200, 50), "Zombie Speed : " + moveSpeed);
    //    GUI.Label(new Rect(1000, 30, 200, 50), "Zombie Damage : " + zombieDamage);
    //    GUI.Label(new Rect(1000, 50, 200, 50), "Zombie Life : " + zombieLife);
    //    GUI.Label(new Rect(1000, 70, 200, 50), "Zombie Jump : " + jumpForce);
    //}
    void Start()
    {
        //zombieLife = 1000;
        //zombieDamage = 25;
        
        z = GameObject.FindGameObjectWithTag("Zombie");
        l = z.GetComponent<Rigidbody2D>();

        rb = GetComponent<Rigidbody2D>();
        land = GameObject.FindGameObjectWithTag("Ground");
        postLand = land.GetComponent<Transform>();
        healthBar.setMaxHealth(zombieLife);
        //Physics.IgnoreCollision(collzombie2.GetComponent<Collider>(), GetComponent<Collider>());
    }
    void Update()
    {
        if(timeBtwAttack <= 0)
        {
            if(Input.GetKey(KeyCode.KeypadEnter))
            {
                
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemy);
                for (int i = 0; i < enemiesToDamage.Length; i++)
                    enemiesToDamage[i].GetComponent<PlayerBehaviour>().TakeDamage();
                if (Menu.sfxOn)
                {
                    FindObjectOfType<AudioManager>().Play("ZombieAttack");
                }

                animator.Play("ZombieAttack");
            }
            timeBtwAttack = startTimeBtwAttack;
            
        }
        else
        {
            timeBtwAttack -= Time.deltaTime;
        }
        if (zombieLife < 0)
        {
            camera.target.Clear();
            camera.target.Add(GameObject.FindGameObjectWithTag("Player").transform);
            zombieLife = 0;
            healthBar.setHealth(zombieLife);
            lifeText.text = zombieLife.ToString();
            Destroy(gameObject);
            humanWin.SetActive(true);
            contButton.SetActive(true);
        }
        lifeText.text = zombieLife.ToString();
    }

    void FixedUpdate()
    {
        moveInput = Input.GetAxis("Horizontal1");
        rb.velocity = new Vector2(moveInput * moveSpeed, rb.velocity.y);
        if (Input.GetKey(KeyCode.UpArrow) && isGrounded)
        {
            if (Menu.sfxOn)
            {
                FindObjectOfType<AudioManager>().Play("ZombieJump");
            }
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            animator.Play("ZombieJump");
            isGrounded = false;
        }
        if (facingRight == false && moveInput < 0)
        {
            Flip();
        }
        if (facingRight == true && moveInput > 0)
        {
            Flip();
        }
        animator.SetFloat("Walking", Mathf.Abs(moveInput));
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            if (Menu.sfxOn)
            {
                FindObjectOfType<AudioManager>().Play("ZombieHurt");
            }
            animator.Play("ZombieHurt");
            zombieLife -= PlayerBehaviour.instance.playerDamage;
            healthBar.setHealth(zombieLife);
            
            Vector2 difference = transform.position - collision.transform.position;
            if (MeaningfulChoice.pistol)
            {
                thrust = 1f;
            }
            if (MeaningfulChoice.shotgun)
            {
                thrust = 2f;
            }
            if (MeaningfulChoice.smg)
            {
                thrust = 1.5f;
            }
            if (MeaningfulChoice.rifle)
            {
                thrust = 3f;
            }
            if (MeaningfulChoice.sniper)
            {
                thrust = 3.5f;
            }
            if (MeaningfulChoice.machinegun)
            {
                thrust = 2.5f;
            }
            difference = difference.normalized * thrust;
            //transform.position = new Vector2(transform.position.x + difference.x, transform.position.y);
            //l.AddForce(difference, ForceMode2D.Impulse);
            //this.GetComponent<Rigidbody2D>().velocity = Vector2.right * 1000f;
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 1000f * difference * thrust);
        }

        if (collision.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(0f, 180f, 0f);
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }
}
