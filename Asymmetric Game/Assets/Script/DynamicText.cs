﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DynamicText : MonoBehaviour
{
    public void Start()
    {
        StartCoroutine(StartCountdown(0.5f));
    }


    public IEnumerator StartCountdown(float countdownValue)
    {
        while (!Matchmaking.matchFound)
        {
            this.gameObject.GetComponent<TextMeshProUGUI>().text = "FINDING YOUR BEST MATCH..";
            yield return new WaitForSeconds(0.5f);
            countdownValue--;
            this.gameObject.GetComponent<TextMeshProUGUI>().text = "FINDING YOUR BEST MATCH...";
            yield return new WaitForSeconds(0.5f);
            countdownValue--;
            this.gameObject.GetComponent<TextMeshProUGUI>().text = "FINDING YOUR BEST MATCH";
            yield return new WaitForSeconds(0.5f);
            countdownValue--;
            this.gameObject.GetComponent<TextMeshProUGUI>().text = "FINDING YOUR BEST MATCH.";
            yield return new WaitForSeconds(0.5f);
            countdownValue--;
            StartCoroutine(StartCountdown(0.5f));
        }
        this.gameObject.GetComponent<TextMeshProUGUI>().text = "MATCH FOUND !";

    }
}
