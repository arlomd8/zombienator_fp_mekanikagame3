﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Matchmaking : MonoBehaviour
{
    public GameObject playButton, Finding;
    public GameObject zbDmg, zbHp, zbSpd, plDmg, plHp, plSpd;
    public static bool matchFound;
    public HealthBar hb;
    private void Start()
    {
        //playButton.SetActive(false);
        matchFound = false;
        //zbDmg.SetActive(false);
        //zbHp.SetActive(false);
        //zbSpd.SetActive(false);
       
    }


    public void Matching()
    {
        Finding.SetActive(true);


        plDmg.GetComponent<TextMeshProUGUI>().text = "Player Damage : " + PlayerBehaviour.instance.playerDamage.ToString();
        plHp.GetComponent<TextMeshProUGUI>().text = "Player Health : " + PlayerBehaviour.instance.playerLife.ToString();
        PlayerBehaviour.instance.moveSpeed = (float)System.Math.Round(PlayerBehaviour.instance.moveSpeed * 100f) / 100f;
        plSpd.GetComponent<TextMeshProUGUI>().text = "Player Speed : " + PlayerBehaviour.instance.moveSpeed.ToString();

        ZombieBehaviour.instance.zombieDamage = Random.Range((PlayerBehaviour.instance.playerDamage),
                                                            (PlayerBehaviour.instance.playerDamage * 2));
        zbDmg.GetComponent<TextMeshProUGUI>().text = "Zombie Damage : " + ZombieBehaviour.instance.zombieDamage.ToString();
    
        ZombieBehaviour.instance.zombieLife = Random.Range((10 * PlayerBehaviour.instance.playerLife - 50),
                                                         (10 * PlayerBehaviour.instance.playerLife + 50));
        zbHp.GetComponent<TextMeshProUGUI>().text = "Zombie Health : " + ZombieBehaviour.instance.zombieLife.ToString();
        hb.setMaxHealth(ZombieBehaviour.instance.zombieLife);

        float fc = Random.Range((PlayerBehaviour.instance.moveSpeed * 1/4), 
                                (PlayerBehaviour.instance.moveSpeed * 3/4));

        ZombieBehaviour.instance.moveSpeed = (float)System.Math.Round(fc * 100f) / 100f;
        zbSpd.GetComponent<TextMeshProUGUI>().text = "Zombie Speed : " + ZombieBehaviour.instance.moveSpeed.ToString();
        //playButton.SetActive(true);
    }
}
