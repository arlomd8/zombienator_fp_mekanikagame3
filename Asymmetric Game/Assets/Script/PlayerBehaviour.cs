﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using System.Threading;
using TMPro;

public class PlayerBehaviour : MonoBehaviour
{
    public static PlayerBehaviour instance;

    [Header("Player Attribute")]
    public static int playerBullet;
    public int playerLife;
    public float moveSpeed;
    public float playerWeight;
    public int playerDamage;
    public float jumpForce;
    public int addLife;
    public int addDamage;
    public float addSpeed;

    [Header("GameObject")]
    public TextMeshProUGUI bulletText;
    public TextMeshProUGUI lifeText;
    public Transform groundCheck;
    private GameObject land;
    private Transform postLand;
    private Rigidbody2D rb;
    public GameObject zombieWin;
    public HealthBar healthBar;
    public Canvas canvasReload;
    public GameObject backButton;

    [Header("Other Thing")]
    public static bool fullBullet = true;
    private float moveInput;
    private float jumpInput;
    public bool isGrounded = true;
    public static bool facingRight = true;
    private Animator animator;
    public MultipleTargetCam camera;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        
    }
    void Start()
    {
        //playerBullet = 30;
        //playerLife = 100;
        zombieWin.SetActive(false);
        rb = GetComponent<Rigidbody2D>();
        land = GameObject.FindGameObjectWithTag("Ground");
        postLand = land.GetComponent<Transform>();
        healthBar.setMaxHealth(playerLife);
        //Physics.IgnoreCollision(collPlayer2.GetComponent<Collider>(), GetComponent<Collider>());
    }
    void Update()
    {
        bulletText.text = playerBullet.ToString() + " x";
        lifeText.text =  playerLife.ToString();
        if (playerLife < 0)
        {
            camera.target.Clear();
            camera.target.Add(GameObject.FindGameObjectWithTag("Zombie").transform);
            playerLife = 0;
            lifeText.text = playerLife.ToString();
            healthBar.setHealth(playerLife);
            Destroy(gameObject);
            zombieWin.SetActive(true);
            backButton.SetActive(true);
        }
    }

    //void OnGUI()
    //{
    //    GUI.Label(new Rect(10, 10, 200, 50), "Player Speed : " + moveSpeed);
    //    GUI.Label(new Rect(10, 30, 200, 50), "Player Damage : " + playerDamage);
    //    GUI.Label(new Rect(10, 50, 200, 50), "Player Bullet : " + playerBullet);
    //    GUI.Label(new Rect(10, 70, 200, 50), "Player Life : " + playerLife);
    //    GUI.Label(new Rect(10, 90, 200, 50), "Player Weight : " + playerWeight);


    //}

    void FixedUpdate()
    {
        
        moveInput = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * (moveSpeed - playerWeight), rb.velocity.y);
        if (Input.GetKey(KeyCode.W) && isGrounded)
        {
            if (Menu.sfxOn)
            {
                FindObjectOfType<AudioManager>().Play("PlayerJump");
            }
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            //animator.SetBool("IsJumping", true);
            isGrounded = false;
            animator.Play("PlayerJump");
        }
        if (facingRight == false && moveInput > 0)
        {
            Flip();
        }
        if (facingRight == true && moveInput < 0)
        {
            Flip();
        }
        animator.SetFloat("Walking", Mathf.Abs(moveInput));
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }
    }
    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(0f, 180f, 0f);
        canvasReload.GetComponent<RectTransform>().Rotate(0f, 180f, 0f);
    }

    public void TakeDamage()
    {
        if (Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("PlayerHurt");
        }
        animator.Play("PlayerHurt");
        if(playerLife < 0)
        {
            playerLife = 0;
        }
        playerLife -= ZombieBehaviour.instance.zombieDamage;
        healthBar.setHealth(playerLife);
    }

    

}
