﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    void Update()
    {
        StartCoroutine("BulletClear");
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Zombie"))
        {
            Destroy(gameObject);
        }
    }
    private IEnumerator BulletClear()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
