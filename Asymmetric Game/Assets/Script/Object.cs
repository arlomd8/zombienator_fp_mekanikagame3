﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Object : MonoBehaviour
{
    public void ButtonClick()
    {
        if (Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("Click");
        }
    }
    public void ButtonDamage()
    {
        PlayerBehaviour.instance.addDamage += FindObjectOfType<Upgrade>().AddDamage();
        SceneManager.LoadScene(0);
    }
    public void ButtonSpeed()
    {
        PlayerBehaviour.instance.addSpeed += FindObjectOfType<Upgrade>().AddSpeed();
        SceneManager.LoadScene(0);
    }
    public void ButtonHealth()
    {
        PlayerBehaviour.instance.addLife += FindObjectOfType<Upgrade>().AddHealth();
        Debug.Log("Health");
        SceneManager.LoadScene(0);
    }

}
