﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Shooter : MonoBehaviour
{
    public GameObject bullet, choice;
    public TextMeshProUGUI timerText;
    public GameObject reloadRadial;     
    public Image reloadImage;
    public float speed;
    private Animator animator;


    private void Start()
    {
        reloadRadial.SetActive(false);
        
        animator = gameObject.GetComponentInParent<Animator>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && PlayerBehaviour.fullBullet == true)
        {
            if (Menu.sfxOn)
            {
                FindObjectOfType<AudioManager>().Play("PlayerAttack");
            }
            GameObject instBullet = Instantiate(bullet, transform.position, Quaternion.identity) as GameObject;
            Rigidbody2D instBulletRigidBody = instBullet.GetComponent<Rigidbody2D>();
            if (PlayerBehaviour.facingRight)
            {
                instBulletRigidBody.AddForce(Vector2.right * speed);
            }
            else
            {
                bullet.GetComponent<Transform>().Rotate(0f, 180f, 0f);
                instBulletRigidBody.AddForce(Vector2.left * speed);
            }
               

            PlayerBehaviour.playerBullet--;
            if (PlayerBehaviour.playerBullet <= 0)
            {
                PlayerBehaviour.fullBullet = false;
                if (Menu.sfxOn)
                {
                    FindObjectOfType<AudioManager>().Play("PlayerReload");
                }
                StartCoroutine("Reload");
            }
            animator.Play("PlayerAttack");
        }
        if(Input.GetKeyDown(KeyCode.Space) && PlayerBehaviour.fullBullet == false)
        {
            if (Menu.sfxOn)
            {
                FindObjectOfType<AudioManager>().Play("PlayerReload");
            }
        }
    }

    private IEnumerator Reload()
    {
        reloadRadial.SetActive(true);
        
        //yield return new WaitForSeconds(3);
       
        if (MeaningfulChoice.pistol)
        {
            float duration = 2f;
            float fixD = duration;
            float totalTime = 0;
            while (totalTime <= fixD)
            {
                reloadImage.fillAmount = totalTime / fixD;
                totalTime += Time.deltaTime;

                //countdownImage.fillAmount = totalTime / duration; //ORIGINAL SCRIPT
                //totalTime += Time.deltaTime;  //ORIGINAL SCRIPT

                duration -= Time.deltaTime;
                var integer = (float)System.Math.Round(duration * 10f) / 10f;
                timerText.text = integer.ToString();
                yield return null;
            }

            PlayerBehaviour.playerBullet = 7;
        }

        if (MeaningfulChoice.shotgun)
        {
            float duration = 2.5f;
            float fixD = duration;
            float totalTime = 0;
            while (totalTime <= fixD)
            {
                reloadImage.fillAmount = totalTime / fixD;
                totalTime += Time.deltaTime;


                duration -= Time.deltaTime;
                var integer = (float)System.Math.Round(duration * 10f) / 10f;
                timerText.text = integer.ToString();
                yield return null;
            }
            PlayerBehaviour.playerBullet = 7;
        }

        if (MeaningfulChoice.smg)
        {
            float duration = 3f;
            float fixD = duration;
            float totalTime = 0;
            while (totalTime <= fixD)
            {
                reloadImage.fillAmount = totalTime / fixD;
                totalTime += Time.deltaTime;


                duration -= Time.deltaTime;
                var integer = (float)System.Math.Round(duration * 10f) / 10f;
                timerText.text = integer.ToString();
                yield return null;
            }

            PlayerBehaviour.playerBullet = 30;
        }

        if (MeaningfulChoice.rifle)
        {
            float duration = 3.5f;
            float fixD = duration;
            float totalTime = 0;
            while (totalTime <= fixD)
            {
                reloadImage.fillAmount = totalTime / fixD;
                totalTime += Time.deltaTime;


                duration -= Time.deltaTime;
                var integer = (float)System.Math.Round(duration * 10f) / 10f;
                timerText.text = integer.ToString();
                yield return null;
            }
            PlayerBehaviour.playerBullet = 30;
        }

        if (MeaningfulChoice.sniper)
        {
            float duration = 4f;
            float fixD = duration;
            float totalTime = 0;
            while (totalTime <= fixD)
            {
                reloadImage.fillAmount = totalTime / fixD;
                totalTime += Time.deltaTime;


                duration -= Time.deltaTime;
                var integer = (float)System.Math.Round(duration * 10f) / 10f;
                timerText.text = integer.ToString();
                yield return null;
            }
            PlayerBehaviour.playerBullet = 10;
        }

        if (MeaningfulChoice.machinegun)
        {
            float duration = 5f;
            float fixD = duration;
            float totalTime = 0;
            while (totalTime <= fixD)
            {
                reloadImage.fillAmount = totalTime / fixD;
                totalTime += Time.deltaTime;


                duration -= Time.deltaTime;
                var integer = (float)System.Math.Round(duration * 10f) / 10f;
                timerText.text = integer.ToString();
                yield return null;
            }
            PlayerBehaviour.playerBullet = 50;
        }

        PlayerBehaviour.fullBullet = true;

        reloadRadial.SetActive(false);
    }
}
