﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knockback : MonoBehaviour
{
    public float thrust;
    public float knockTime;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Zombie"))
        {
            Rigidbody2D zombie = other.GetComponent<Rigidbody2D>();
            if(zombie!= null)
            {
                zombie.isKinematic = false;
                Vector2 difference = zombie.transform.position - transform.position;
                difference = difference.normalized * thrust;
                zombie.AddForce(difference, ForceMode2D.Impulse);
                //zombie.isKinematic = true;
                StartCoroutine("KnockCo()");
            }
        }
    }

    private IEnumerator KnockCo(Rigidbody2D zombie)
    {
        if(zombie!= null)
        {
            yield return new WaitForSeconds(knockTime);
            zombie.velocity = Vector2.zero;
            zombie.isKinematic = true;
        }
    }
}
