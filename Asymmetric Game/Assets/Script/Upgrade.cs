﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Upgrade : MonoBehaviour
{
    public static Upgrade instance;
    public int upDmg, upHp;
    public float upSpd;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }
    public int AddDamage()
    {
        return upDmg += 10;
    }

    public int AddHealth()
    {
        return upHp += 50;
    }

    public float AddSpeed()
    {
        return upSpd += 5;
        
    }
}
