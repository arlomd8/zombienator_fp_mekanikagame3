﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Entity
{
    public float moveSpeed;
    public float health;
    public float jumpForce;

};


