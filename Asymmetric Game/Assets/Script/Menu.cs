﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public TextMeshProUGUI sfxText;
    public TextMeshProUGUI bgmText;
    public static bool sfxOn;
    public static bool bgmOn;

    // public AudioScript audioOpt;
    void Start()
    {
        sfxOn = true;
        bgmOn = true;
        sfxText.text = "ON";
        bgmText.text = "ON";

        if (Menu.bgmOn)
        {
            FindObjectOfType<AudioManager>().Play("Theme");
        }
        //FindObjectOfType<AudioManager>().Play("Theme");
    }
    public void PlayGame()
    {
        //FindObjectOfType<AudioManager>().Play("Theme");
        SceneManager.LoadScene(1);
    }
    public void BGMClick()
    {
        if (bgmOn)
        {
            bgmText.text = "OFF";
            bgmOn = false;
            FindObjectOfType<AudioManager>().Mute("Theme");
        }
        else
        {
            //FindObjectOfType<AudioManager>().Play("Click");
            bgmText.text = "ON";
            bgmOn = true;
            FindObjectOfType<AudioManager>().UnMute("Theme");
        }
    }
    public void SFXClick()
    {
        if (sfxOn)
        {
            sfxText.text = "OFF";
            sfxOn = false;
        }
        else
        {
            //FindObjectOfType<AudioManager>().Play("Click");
            sfxText.text = "ON";
            sfxOn = true;
        }
    }

    public void ButtonClick()
    {
        if(sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("Click");
        }
    }

    public void QuitGame()
    {
        Application.Quit();
        
    }
}
