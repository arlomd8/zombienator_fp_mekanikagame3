﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
public class MeaningfulChoice : MonoBehaviour
{
    public GameObject choicePanel, matchmakingPanel, confirmationPanel;
    public static bool pistol, smg, rifle, shotgun, machinegun, sniper;
    public TextMeshProUGUI pistolDesc, sgDesc, smgDesc, arDesc, awpDesc, mgDesc;
    public Upgrade upgrade;
    public HealthBar hb;


    [Header("Pistol")]
    [SerializeField] private int pistolDamage;
    [SerializeField] private int pistolAmmo;
    [SerializeField] private int pistolWeight;

    [Header("Shotgun")]
    [SerializeField] private int sgDamage;
    [SerializeField] private int sgAmmo;
    [SerializeField] private int sgWeight;

    [Header("SMG")]
    [SerializeField] private int smgDamage;
    [SerializeField] private int smgAmmo;
    [SerializeField] private int smgWeight;

    [Header("Rifle")]
    [SerializeField] private int arDamage;
    [SerializeField] private int arAmmo;
    [SerializeField] private int arWeight;

    [Header("Sniper")]
    [SerializeField] private int awpDamage;
    [SerializeField] private int awpAmmo;
    [SerializeField] private int awpWeight;

    [Header("Machine Gun")]
    [SerializeField] private int mgDamage;
    [SerializeField] private int mgAmmo;
    [SerializeField] private int mgWeight;



    public void Start()
    {
        pistol = false;
        smg = false;
        rifle = false;
        shotgun = false;
        machinegun = false;
        sniper = false;
        pistolDesc.text = "Damage : " + pistolDamage + "\nAmmo : " + pistolAmmo + "\nWeight : " + pistolWeight;
        sgDesc.text = "Damage : " + sgDamage + "\nAmmo : " + sgAmmo + "\nWeight : " + sgWeight;
        smgDesc.text = "Damage : " + smgDamage + "\nAmmo : " + smgAmmo + "\nWeight : " + smgWeight;
        arDesc.text = "Damage : " + arDamage + "\nAmmo : " + arAmmo + "\nWeight : " + arWeight;
        awpDesc.text = "Damage : " + awpDamage + "\nAmmo : " + awpAmmo + "\nWeight : " + awpWeight;
        mgDesc.text = "Damage : " + mgDamage + "\nAmmo : " + mgAmmo + "\nWeight : " + mgWeight;
        upgrade = GameObject.Find("Upgrade").GetComponent<Upgrade>();

        PlayerBehaviour.instance.addLife = upgrade.upHp;
        PlayerBehaviour.instance.addDamage = upgrade.upDmg;
        PlayerBehaviour.instance.addSpeed = upgrade.upSpd;

    }
    public void Pistol()
    {
        pistol = true;
        PlayerBehaviour.instance.playerDamage = pistolDamage + PlayerBehaviour.instance.addDamage;
        PlayerBehaviour.playerBullet = pistolAmmo;
        PlayerBehaviour.instance.playerWeight = pistolWeight;
        PlayerBehaviour.instance.playerLife = 100 + PlayerBehaviour.instance.addLife;
        PlayerBehaviour.instance.moveSpeed = 5 + PlayerBehaviour.instance.addSpeed;
        //choicePanel.SetActive(false);
        hb.setMaxHealth(PlayerBehaviour.instance.playerLife);
        confirmationPanel.SetActive(true);

    }

    public void Shotgun()
    {
        shotgun = true;
        PlayerBehaviour.instance.playerDamage = sgDamage + PlayerBehaviour.instance.addDamage;
        PlayerBehaviour.playerBullet = sgAmmo;
        PlayerBehaviour.instance.playerWeight = sgWeight;
        PlayerBehaviour.instance.playerLife = 100 + PlayerBehaviour.instance.addLife;
        PlayerBehaviour.instance.moveSpeed = 5 + PlayerBehaviour.instance.addSpeed;
        //choicePanel.SetActive(false);
        hb.setMaxHealth(PlayerBehaviour.instance.playerLife);
        confirmationPanel.SetActive(true);

    }

    public void SMG()
    {
        smg = true;
        PlayerBehaviour.instance.playerDamage = smgDamage + PlayerBehaviour.instance.addDamage;
        PlayerBehaviour.playerBullet = smgAmmo;
        PlayerBehaviour.instance.playerWeight = smgWeight;
        PlayerBehaviour.instance.playerLife = 100 + PlayerBehaviour.instance.addLife;
        PlayerBehaviour.instance.moveSpeed = 5 + +PlayerBehaviour.instance.addSpeed;
        //choicePanel.SetActive(false);
        hb.setMaxHealth(PlayerBehaviour.instance.playerLife);
        confirmationPanel.SetActive(true);

    }

    public void Rifle()
    {
        rifle = true;
        PlayerBehaviour.instance.playerDamage = arDamage + PlayerBehaviour.instance.addDamage;
        PlayerBehaviour.playerBullet = arAmmo;
        PlayerBehaviour.instance.playerWeight = arWeight;
        PlayerBehaviour.instance.playerLife = 100 + PlayerBehaviour.instance.addLife;
        PlayerBehaviour.instance.moveSpeed = 5 + PlayerBehaviour.instance.addSpeed;
        //choicePanel.SetActive(false);
        hb.setMaxHealth(PlayerBehaviour.instance.playerLife);
        confirmationPanel.SetActive(true);

    }

    public void Sniper()
    {
        sniper = true;
        PlayerBehaviour.instance.playerDamage = awpDamage + PlayerBehaviour.instance.addDamage;
        PlayerBehaviour.playerBullet = awpAmmo;
        PlayerBehaviour.instance.playerWeight = awpWeight;
        PlayerBehaviour.instance.playerLife = 100 + PlayerBehaviour.instance.addLife;
        PlayerBehaviour.instance.moveSpeed = 5 + PlayerBehaviour.instance.addSpeed;
        //choicePanel.SetActive(false);
        hb.setMaxHealth(PlayerBehaviour.instance.playerLife);
        confirmationPanel.SetActive(true);

    }
    public void MachineGun()
    {
        machinegun = true;
        PlayerBehaviour.instance.playerDamage = mgDamage + PlayerBehaviour.instance.addDamage;
        PlayerBehaviour.playerBullet = mgAmmo;
        PlayerBehaviour.instance.playerWeight = mgWeight;
        PlayerBehaviour.instance.playerLife = 100 + PlayerBehaviour.instance.addLife;
        PlayerBehaviour.instance.moveSpeed = 5 + PlayerBehaviour.instance.addSpeed;
        //choicePanel.SetActive(false);
        hb.setMaxHealth(PlayerBehaviour.instance.playerLife);
        confirmationPanel.SetActive(true);

    }




}
